import { NgModule, ModuleWithProviders, InjectionToken, Optional } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatSelectModule } from '@angular/material/select';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { CrSelectComponent } from './cr-select.component';
import { CrSelectService } from './cr-select.service';

@NgModule({
  declarations: [CrSelectComponent],
  imports: [
    CommonModule,
    HttpClientModule,
    FormsModule,
    MatSelectModule,
    MatFormFieldModule,
    MatInputModule,
    ReactiveFormsModule
  ],
  providers: [CrSelectService],
  exports: [CrSelectComponent]
})
export class CrSelectModule {
  static forRoot(config: any): ModuleWithProviders {
    return {
      ngModule: CrSelectModule,
      providers: [
        {
          provide: 'config',
          useValue: config
        }
      ]
    };
  }

  static forChild(config: any): ModuleWithProviders {
    return {
      ngModule: CrSelectModule,
      providers: [
        {
          provide: 'config',
          useValue: config
        }
      ]
    };
  }
}

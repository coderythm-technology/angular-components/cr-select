import {
  Component,
  OnInit,
  Input,
  EventEmitter,
  Output,
  forwardRef,
  ChangeDetectorRef,
  AfterViewInit,
  OnChanges
} from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { CrSelectService } from './cr-select.service';
// import * as _ from 'lodash';
// import { environment } from '../../../../environments/environment';
import { NG_VALUE_ACCESSOR, ControlValueAccessor } from '@angular/forms';
import { FormControl } from '@angular/forms';
import { Subject } from 'rxjs';
declare var $: any;

const noop = () => { };
/* tslint:disable */
export const CUSTOM_INPUT_CONTROL_VALUE_ACCESSOR: any = {
  provide: NG_VALUE_ACCESSOR,
  useExisting: forwardRef(() => CrSelectComponent),
  multi: true
};
/* tslint:enable */

@Component({
  selector: 'cr-select',
  templateUrl: './cr-select.component.html',
  styleUrls: ['./cr-select.component.scss'],
  providers: [CUSTOM_INPUT_CONTROL_VALUE_ACCESSOR, CrSelectService]
})
export class CrSelectComponent implements OnInit, ControlValueAccessor, AfterViewInit, OnChanges {
  public options: any = {
    data: []
  };
  selectOnce = true;
  startValue: any;
  value: any;
  query: any;
  public innerValue: any;
  public innerValueLabel: any;
  // @Input() url: string;
  public url: string;
  @Input() multiple: boolean;
  @Input() defaultText: string;
  @Input() type: string;
  @Input() isEntity: string;
  @Input() entity: string;
  @Input() specialty: string;
  @Input() placeholder: string;
  @Input() params: any;
  @Input() isI18n: string;
  @Input() appearance: string = 'standard';
  @Input() defaultOptions: boolean;
  @Input() reset: boolean;
  @Input() values: Array<object>;
  @Input() selected: Array<object>;
  @Input() libraryIds: Array<string>;
  @Input() disabled: boolean;
  @Output() valueChange: EventEmitter<any> = new EventEmitter();
  @Input() triggerChange: Subject<boolean>;
  option: any = [];
  typeObject: any = {};
  optionSelected: any;
  allowClear = true;
  // multiple = true;
  private onTouchedCallback: () => void = noop;
  private onChangeCallback: (_: any) => void = noop;
  // public url = environment.api_base_url;
  myControl = new FormControl();
  writeValue(value: any) {
    setTimeout(() => {
      this.getData();
    }, 10);
    if (value !== this.innerValue) {
      if (value && value._id) {
        this.innerValue = value._id;
      } else {
        this.innerValue = value;
      }
    }
    this.cdr.detectChanges();
  }

  registerOnChange(fn: any) {
    this.onChangeCallback = fn;
  }

  registerOnTouched(fn: any) {
    this.onTouchedCallback = fn;
  }

  constructor(
    public http: HttpClient,
    private cdr: ChangeDetectorRef,
    private crSelectService: CrSelectService) {
    this.url = crSelectService.serverUrl;
  }


  ngOnInit() {
    this.url = this.crSelectService.serverUrl;
    if (this.selected) {
      this.myControl.setValue(this.selected);
    }
    if (this.triggerChange) {
      this.triggerChange.subscribe(v => {
        if (this.type === 'array') {
          this.setArrayData();
        } else if (this.type === 'ajax') {
          this.fetchData();
        }
        if (v !== null || v !== undefined) {
          setTimeout(() => {
            console.log('triggered', v);
            this.myControl.setValue(v);
          }, 10);
        }
      });
    }
  }

  change(value) {
    setTimeout(() => {
      this.onChangeCallback(this.myControl.value);
      this.valueChange.emit(this.myControl.value);
      this.cdr.detectChanges();
    }, 300);
  }


  ngAfterViewInit() {
    if (this.type === 'array') {
      this.setArrayData();
    } else if (this.type === 'ajax') {
      this.fetchData();
    }
  }

  ngOnChanges() {
    if (this.reset) {
      this.myControl.setValue(null);
    }
    if (this.type === 'array') {
      this.setArrayData();
    } else if (this.type === 'ajax') {
      this.fetchData();
    }

  }

  setArrayData() {
    const multipleOption = [];
    this.values.forEach((value: any) => {
      const text = value.name || value.label || value.title || value.firstname || value[this.defaultText];
      // if (value[this.defaultText]) {
      //   textValue = value[this.defaultText];
      // }
      multipleOption.push({ id: value._id, text });
    });
    setTimeout(() => {
      this.options.data = multipleOption;
    });
  }

  getData() {
    if (this.entity && this.innerValue && !this.options.multiple) {
      const url = this.url + '/' + this.entity + '/' + this.innerValue;
      this.http.get(url).subscribe(
        (data: any) => {
          const multipleOption = [];
          if (data && data.body) {
            let text = data.body.name || data.body.label || data.body.title || data.body.firstname || data.body[this.defaultText];
            // if (data.body[this.defaultText]) {
            //   text = data.body[this.defaultText];
            // }
            if (typeof text === 'object') {
              text = text[this.isI18n || 'en'];
            }
            multipleOption.push({
              id: data.body._id,
              text
            });
            setTimeout(() => {
              this.options.data = multipleOption;
            });
          } else {
            console.warn('error');
          }
        },
        err => {
          console.warn('error');
        }
      );
    } else if (this.entity && this.innerValue && this.option.multiple) {
      const multipleOption = [];
      const innerValues = [];
      const innerValue = this.innerValue;
      if (this.options.data.length === 0) {
        this.innerValue.forEach(item => {
          const url = this.url + '/' + this.entity + '/' + item;
          this.http.get(url).subscribe(
            (data: any) => {
              if (data && data.body) {
                // multipleOption.push({ id: data['body']._id,
                // text: data['body'].name ||  data['body'].label || data['body'].title });
                innerValues.push(item);
                let ifExist = false;
                multipleOption.forEach(option => {
                  if (option.id === data.body._id) {
                    ifExist = true;
                  }
                });
                if (!ifExist) {
                  const text = data.body.name || data.body.label || data.body.title || data.body.firstname || data.body[this.defaultText];
                  // if (data.body[this.defaultText]) {
                  //   text = data.body[this.defaultText];
                  // }
                  multipleOption.push({
                    id: data.body._id,
                    text
                  });
                  setTimeout(() => {
                    // if (this.options.data.length === 0) {
                    this.options.data = [];
                    this.options.data = multipleOption;
                    this.innerValue = innerValue;
                    // }
                  });
                }
              }
            },
            err => {
              innerValues.push(item);
              // callback([]);
            }
          );
        });
      }
    }
  }

  fetchData() {
    const that = this;
    this.http.get<any>(this.url + '/' + this.entity, { params: this.params }).
      subscribe(
        (res) => {
          const arr = [];

          // $.each(res.body, function (index, value) {
          res.body.forEach((value) => {
            let text = '';
            const possibilities = ['name', 'label', 'username', 'title', 'firstname', this.defaultText];
            possibilities.forEach(possible => {
              if (value[possible]) {
                if (that.isI18n) {
                  text = value[possible][that.isI18n];
                } else {
                  text = value[possible];
                }

              }
            });
            arr.push({
              id: value._id,
              text
            });
            if (this.innerValue && value._id === this.innerValue) {
              this.innerValueLabel = text;
            }
            // arr.sort((a, b) => {
            //     if (a.text && b.text) {
            //         return a.text.localeCompare(b.text);
            //     } else {
            //         return a;
            //     }
            // });
            setTimeout(() => {
              that.options.data = arr;
            });
          });
        },
        (error: any) => {

        }
      );
  }
}


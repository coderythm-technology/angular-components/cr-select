import { Inject, Injectable } from '@angular/core';

@Injectable()
export class CrSelectService {

    serverUrl = '';

    constructor(@Inject('config') private config: any) {
        if (config.url) {
            this.serverUrl = config.url;
        }
    }
}

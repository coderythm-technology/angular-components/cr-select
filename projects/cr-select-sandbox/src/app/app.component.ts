import { Component } from '@angular/core';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'cr-select-sandbox';
  selectedType = '5dc93a2c7943b13bc4eb96d6';

  triggerSelectValue: Subject<any> = new Subject();

  triggerSelect() {
    this.triggerSelectValue.next('5e4e5cf3b67a780a73aa6803');
  }
}
